#ifndef MINESWEEPER_H
#define MINESWEEPER_H

#include <QMainWindow>
#include <QPushButton>
#include <zone.h>
#include <QLabel>
namespace Ui {
class Minesweeper;
}
const int X= 30, Y = 24;
class Minesweeper : public QMainWindow
{
    Q_OBJECT

public:
    explicit Minesweeper(QWidget *parent = 0);
    ~Minesweeper();

    void generateNewField();
    void incrementZoneValue(int x, int y);
signals:
    void revealMines();


public slots:
    void updateField();
    void gameOver();
    void checkIfAllMinesRevealed();

    void incrementMinesLeft();
    void decrementMinesLeft();

    void playAgain();

private:
    Ui::Minesweeper *ui;
    Zone* field[X][Y];
    bool minesGenerated;//flag whether mines are generated

    QLabel* mines_left_l;
    QLabel* win_lose_l;
    int mines_left;
    int numZonesRevealed;

    QPushButton* play_again_button;

};

#endif // MINESWEEPER_H
