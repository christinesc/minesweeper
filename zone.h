#ifndef ZONE_H
#define ZONE_H

/*
 *  30x24 with 667 mines
 *
 * Zone properties:
 *  state: whether it contains a mine
 *  revealed: whether the user has opened a zone
 *  marked: whether the user has marked the zone as containing a mine
 *  zoneValue: number of mines in surrounding zones
 *  xcoor: x coordinate
 *  ycoor: y coordinate
 *
 * First zone revealed is guaranteed to not contain a mine. Field is generated after the first zone revealed
 * Autoclear all zones ==0 when a zone with value 0 is revealed
 **/

#include <QPushButton>
#include <QString>
#include <QMouseEvent>
#include <QPalette>

struct Point {
    int x, y;
};

class Zone : public QPushButton
{
    Q_OBJECT
public:

    Zone(int x, int y, QWidget*parent = 0);
    virtual ~Zone();

    bool hasMine() const;
    bool isFirstZoneRevealed() const;

    void addMine();
    void incrementZoneValue(int);
signals:
    void leftClicked();
    void rightClicked();

    void zoneValueIsZero();
    void revealSuccess();
    void hitMine();

    void markedZone();
    void unmarkedZone();

    void firstZoneRevealed();
public slots:
    void toggleMarked();
    void revealZone();

    void autoClear();

protected:
    void mouseReleaseEvent(QMouseEvent *e) {
        //emit clicked(e);
        if(e->button() == Qt::RightButton) emit rightClicked();
        else if(e->button() == Qt::LeftButton) emit leftClicked();
    }

private:
    bool firstZoneHasBeenRevealed;
    bool state;
    bool revealed, marked;
    int zoneValue;
    QString revealedText;
    //int xPos, yPos;
    Point position;
};

#endif // ZONE_H
