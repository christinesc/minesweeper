#include "minesweeper.h"
#include "ui_minesweeper.h"
#include <cstdlib>
/*
 *  30x24 with NUM_MINES mines
 *
 * Zone properties:
 *  state: whether it contains a mine
 *  revealed: whether the user has opened a zone
 *  marked: whether the user has marked the zone as containing a mine
 *  zoneValue: number of mines in surrounding zones
 *  xcoor: x coordinate
 *  ycoor: y coordinate
 *
 * First zone revealed is guaranteed to not contain a mine. Field is generated after the first zone revealed
 * Autoclear all surrounding zones when a zone with value 0 is revealed
 **/

const int NUM_MINES = 200;

bool isValidPosition(int x, int y) {
    return (x >= 0 && x < X) && (y >= 0 && y < Y);
}

void Minesweeper::generateNewField() {
    //Set up field
    for(int i = 0; i < X; i++) {
        for (int j = 0; j < Y; j++) {
            field[i][j] = new Zone(i, j, this);
            Zone* temp = field[i][j];

            temp->setFixedHeight(40);
            temp->setFixedWidth(40);
            temp->move(100 +45*i, 100 +45*j);

            connect(temp, SIGNAL(firstZoneRevealed()), this, SLOT(updateField()));


            connect(temp, SIGNAL(leftClicked()), temp, SLOT(revealZone()));
            connect(temp, SIGNAL(rightClicked()), temp, SLOT(toggleMarked()));

            connect(temp, SIGNAL(markedZone()), this, SLOT(decrementMinesLeft()));
            connect(temp, SIGNAL(unmarkedZone()), this, SLOT(incrementMinesLeft()));

            //determines when game is lost
            connect(temp, SIGNAL(hitMine()), this, SLOT(gameOver()));

            //Determines when game is won
            connect(temp, SIGNAL(revealSuccess()), this, SLOT(checkIfAllMinesRevealed()));
            temp->show();

        }
    }
    //autoclear when zone value is zero
    for(int i = 0; i < X; i++) {
        for(int j = 0; j < Y; j++) {
            Zone* temp = field[i][j];
            int x= i-1, y = j-1;
            if(isValidPosition(x, y))
                connect(temp, SIGNAL(zoneValueIsZero()), field[x][y], SLOT(autoClear()));

            if(isValidPosition(i-1, j))
                connect(temp, SIGNAL(zoneValueIsZero()), field[i-1][j], SLOT(autoClear()));
            if(isValidPosition(i-1, j+1))
                connect(temp, SIGNAL(zoneValueIsZero()), field[i-1][j+1], SLOT(autoClear()));

            if(isValidPosition(i, j-1))
                connect(temp, SIGNAL(zoneValueIsZero()), field[i][j-1], SLOT(autoClear()));
            if(isValidPosition(i, j+1))
                connect(temp, SIGNAL(zoneValueIsZero()), field[i][j+1], SLOT(autoClear()));

            if(isValidPosition(i+1, j-1))
                connect(temp, SIGNAL(zoneValueIsZero()), field[i+1][j-1], SLOT(autoClear()));
            if(isValidPosition(i+1, j))
                connect(temp, SIGNAL(zoneValueIsZero()), field[i+1][j], SLOT(autoClear()));
            if(isValidPosition(i+1, j+1))
                connect(temp, SIGNAL(zoneValueIsZero()), field[i+1][j+1], SLOT(autoClear()));
        }
    }

}

Minesweeper::Minesweeper(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Minesweeper)
{
    ui->setupUi(this);
    this->setFixedHeight(1500);
    this->setFixedWidth(1550);
    minesGenerated = false;

    generateNewField();

    //Mines left label
    mines_left_l = new QLabel(this);
    mines_left = NUM_MINES;
    mines_left_l->setFixedWidth(500);
    mines_left_l->move(100, 50);

    QString label("Number of Mines left: ");
    label += QString::number(mines_left);
    mines_left_l->setText(label);

    numZonesRevealed = 0;

    //Win/loss label
    win_lose_l = new QLabel(this);
    win_lose_l->setFixedWidth(500);
    win_lose_l->move(750, 1300);
    win_lose_l->hide();

    //Play Again button
    play_again_button = new QPushButton(this);
    play_again_button->move(750-25, 1400);
    play_again_button->setFixedWidth(150);
    play_again_button->setFixedHeight(50);
    play_again_button->setText(QString("Play Again"));
    play_again_button->hide();

    connect(play_again_button, SIGNAL(released()), this, SLOT(playAgain()));


}


Minesweeper::~Minesweeper()
{
    //deallocate field
    for(int i = 0; i < 30; i++) {
        for (int j = 0; j < 24; j++) {
            delete field[i][j];
        }
    }
    delete ui;
    delete win_lose_l;
    delete mines_left_l;
    delete play_again_button;
}

void Minesweeper::incrementZoneValue(int x, int y) {
    if(!isValidPosition(x, y))
        return;
    field[x][y]->incrementZoneValue(1);
}


void Minesweeper::updateField() {
    if(!minesGenerated) {
        for(int i = 0; i < NUM_MINES; i++) {
            int random_mine = rand()%(X*Y);
            int x = random_mine%X;
            int y = random_mine/X;
            if(field[x][y]->isFirstZoneRevealed()) {
                //zone first revealed is protected from mines
                i--;
            }
            else if(field[x][y]->hasMine()) {
                i--;
            }
            else {
                field[x][y]->addMine();

                //If game over, reveal all mines
                connect(this, SIGNAL(revealMines()), field[x][y], SLOT(revealZone()));

                //update value of other zones
                incrementZoneValue(x-1, y-1);
                incrementZoneValue(x-1, y+0);
                incrementZoneValue(x-1, y+1);
                incrementZoneValue(x+0, y-1);
                incrementZoneValue(x+0, y+1);
                incrementZoneValue(x+1, y-1);
                incrementZoneValue(x+1, y+0);
                incrementZoneValue(x+1, y+1);
            }

        }
    }
    minesGenerated = true;
}

void Minesweeper::incrementMinesLeft() {
    mines_left++;
    QString label("Number of Mines left: ");
    label += QString::number(mines_left);
    mines_left_l->setText(label);
}
void Minesweeper::decrementMinesLeft() {
    mines_left--;
    QString label("Number of Mines left: ");
    label += QString::number(mines_left);
    mines_left_l->setText(label);
}

void Minesweeper::gameOver() {
    emit revealMines();
    win_lose_l->setText(QString("You Lose!"));
    win_lose_l->show();
    play_again_button->show();

}
void Minesweeper::checkIfAllMinesRevealed() {
    numZonesRevealed++;
    if(numZonesRevealed == X*Y-200) {
        //game is won, all zones besides one with mines are revealed
        win_lose_l->setText(QString("You Win!"));
        win_lose_l->show();
        play_again_button->show();
    }
}

void Minesweeper::playAgain() {
    //deallocate field
    for(int i = 0; i < 30; i++) {
        for (int j = 0; j < 24; j++) {
            delete field[i][j];
        }
    }
    //new field
    generateNewField();

    //reset all 3 labels
    mines_left = 200;
    QString label("Number of Mines left: ");
    label += QString::number(mines_left);
    mines_left_l->setText(label);
    numZonesRevealed = 0;

    win_lose_l->hide();

    play_again_button->hide();

    minesGenerated = false;

}
