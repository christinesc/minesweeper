#include "zone.h"


Zone::Zone(int x, int y, QWidget *parent): QPushButton(parent){
    position.x = x; position.y = y;
    state = false;
    revealed = false;
    marked = false;
    setText(QString("."));

    //updates after first zone is revealed
    zoneValue = 0;
    firstZoneHasBeenRevealed = false;
}

Zone::~Zone() {
    //delete zone_button;
}

bool Zone::hasMine() const {
    return state;
}

bool Zone::isFirstZoneRevealed() const {
    return firstZoneHasBeenRevealed;
}
void Zone::toggleMarked() {
    if(revealed)
        return;

    if(marked) { //marked -> unmarked
        setText(QString("-"));
        emit unmarkedZone();
    }
    else { //unmarked -> marked
        setText(QString("X"));
        emit markedZone();
    }
    marked = !marked;
}

void Zone::revealZone() {
    //If is first zone revealed, generate mines
    if(!firstZoneHasBeenRevealed) {
        firstZoneHasBeenRevealed = true;
        emit firstZoneRevealed();
    }


    //Don't reveal if it is marked
    if(marked) return;

    //Nothing happens if it is already revealed
    if(revealed)  return;

    revealed = !revealed;

    if(hasMine()) {
        revealedText = "!";
        emit hitMine();
    }
    else if(zoneValue == 0) {
        revealedText = " ";
        emit zoneValueIsZero();
    }
    else {
        revealedText = char(zoneValue+48);
    }
    setText(QString(revealedText));

    emit revealSuccess();
}

void Zone::autoClear() {
    if(marked) {
        toggleMarked();
    }
    revealZone();
}

void Zone:: addMine() {
    state = true;
}

void Zone::incrementZoneValue(int i = 1) {
    zoneValue += i;
}


