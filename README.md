PIC 10C FINAL PROJECT

This is a very basic Minesweeper application.

There is a 30x24 field that contains 720 zones. 200 of these zones each contain a mine.
The goal of the game is to mark all of the zones containing a mine and reveal all zones that don't.
The first zone revealed is guaranteed to not contain a mine. When revealed, a zone will either be empty, contain a number, or an "X".


A "." denotes a zone that has not been revealed
A "X" means that the zone is marked as not containing a mine.
An "!" denotes that you have revealed a mine, and thus has lost.
Otherwise, the number of the mine reveals how many zones surround it has a mine.
A " " means that the 8 mines surrounding this mine do not contain any mines.

To mark a zone as containing a mine, right-click the zone. Similarly, to unmark it, right-click it.
To reveal a zone, left-click it.